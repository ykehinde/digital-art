import React, { Component } from 'react';
import './Reset.css';
import './App.css';
import axios from 'axios';


class App extends Component {
  constructor(props) {
    super(props)
    
    this.state= {
      data: [],
      photo: '',
      user: '',
    }
    this.getPhoto = this.getPhoto.bind(this);
  }

  componentWillMount() {
    this.callApi()
    this.getPhoto()
  }
  
  callApi() {
    axios.get('https://api.unsplash.com/photos/?client_id=6f2602a740086e4666282beddd848551bc55cfb33a33ed425581630ac11a506f&orientation=portrait')
      .then(res => {
        this.setState({
          data: res.data
        })
      })
  }

  getPhoto() {
    
    setInterval(() => {
      // console.log(this.state.data);
      let image = this.state.data[Math.floor(Math.random() * this.state.data.length)]
      console.log(image);
      
      this.setState({
        photo: image.urls.full,
        user: image.user.name
      })
      
    }, 3000);
  }


  render() {
    return (
      <div className="App">
        <div className="frame">
          {/* <img className="photo" src={this.state.photo} alt=""/> */}
          <img src = "https://images.pexels.com/photos/204725/pexels-photo-204725.jpeg"alt = "placeholder" />
          <h2 className="user">{this.state.user}</h2></div>
      </div>
    );
  }
}

export default App;
